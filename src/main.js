require("dotenv").config();

const logger = require("../libs/logger");
const cron = require("node-schedule");
const bb = require("bluebird");
const db = require("../libs/db");
const helper = require("../libs/helper");
const SegfaultHandler = require("segfault-handler");

exports.main = function() {
	SegfaultHandler.registerHandler("crash.log");

	const cronSchedule = process.env.CRON_SCHEDULE;
	db.initDb()
		.catch(function(err) {
			logger.error("Unable to connect to the db", err);
			process.exit(1);
		})
		.then(function() {
			if (cronSchedule === "once") {
				logger.info("Running job once");
				run();
			} else {
				logger.info("Running job on schedule: " + cronSchedule);
				cron.scheduleJob(cronSchedule, run);
			}
		});
};

if (require.main === module) {
	exports.main();
}

process.once("SIGTERM", terminate);
process.once("SIGINT", terminate);

function terminate() {
	logger.error("Received Termination Signal");
	db.closeDb()
		.then(function() {
			logger.info("Terminate: Database closed");
		})
		.catch(function(err) {
			logger.error("Terminate: Unable to close the database ", err);
		})
		.finally(function() {
			process.exit(0);
		});
}

function run() {
	logger.info("Start of processing");
	processCourses()
		.then(function() {
			logger.info("Finished");
		})
		.then(function() {
			if (process.env.CRON_SCHEDULE === "once") {
				db.closeDb()
					.then(function() {
						logger.info("All database connections closed");
					})
					.catch(function(error) {
						logger.info("Problem closing all database connections");
					})
					.then(function() {
						logger.info("Done");
						process.exit(0);
					});
			}
		})
		.catch(function(error) {
			logger.error("Unknown error ", error);
		});
}

async function processCourses() {
	for (let i = 1; i < 8; i++) {
		const templateId = process.env["LMS_CONVERTED_COURSES_TEMPLATE_UMS0" + i];
		await processCoursesByCampus("UMS0" + i, templateId);
	}

	return Promise.resolve();
}

function processCoursesByCampus(campus, templateId) {
	return new Promise(function(resolve, reject) {
		const campusesToRun = process.env.CAMPUSES_TO_RUN || "all";

		if (campusesToRun !== "all" && !campusesToRun.includes(campus)) {
			logger.info("As requested, skipping " + campus);
			return resolve();
		}

		logger.info("Start of processing for " + campus);

		getCoursesFromLMS(campus, templateId)
			.then(function(courses) {
				return bb.map(
					courses,
					function(course) {
						return processCourse(course.id, course.code.trim());
					},
					{
						concurrency: Number(process.env.CALLS_LIMIT_COURSES)
					}
				);
			})
			.then(function() {
				logger.info("End of processing for " + campus);
			})
			.catch(function(err) {
				logger.warn(
					"Error processing courses for " + campus + " ::: " + templateId
				);
			})
			.finally(function() {
				resolve();
			});
	});
}

function getCoursesFromLMS(campus, templateId) {
	return new Promise(function(resolve, reject) {
		helper.axios
			.get("/course/by-template/" + templateId)
			.then(function(res) {
				resolve(res.data.courses);
			})
			.catch(function(err) {
				logger.warn(
					"Error getting courses for " + campus + " ::: " + templateId,
					helper.handleAxiosError(err)
				);
				resolve();
			});
	});
}

function processCourse(lmsId, code) {
	return new Promise(function(resolve, reject) {
		if (code.endsWith("_BB")) {
			logger.info("Clipping " + code);
			code = code.substr(0, code.length - 3);
		}
		const regex = /^\d{4}\.UMS\d{2}-[C|S]\.\d+/;
		if (!regex.test(code)) {
			logger.info("Skipped " + lmsId + " due to non standard code, " + code);

			return resolve();
		}

		getCourseDataFromMaineStreet(code)
			.then(function(courseData) {
				if (courseData.length === 0) {
					throw new Error("Course not found in MaineStreet");
				}
				return updateCourseInLMS(lmsId, code, courseData[0].TITLE);
			})
			.then(function() {
				return getInstructorsFromMaineStreet(code);
			})
			.then(function(instructors) {
				return bb.map(
					instructors,
					function(instructor) {
						return enrollInstructorIntoCourse(
							lmsId,
							instructor.USER_LMS_ID,
							instructor.TYPE
						);
					},
					{
						concurrency: Number(process.env.CALLS_LIMIT_LMS_API)
					}
				);
			})
			.catch(function(err) {
				logger.warn("Problem processing course ", {
					lmsId: lmsId,
					code: code,
					error: err.message
				});
			})
			.finally(function() {
				resolve();
			});
	});
}

function getCourseDataFromMaineStreet(courseId) {
	let sql = "Select title From lms_course_info ";
	const where = getCourseWhere(courseId);

	return db.queryDb(sql + where.clause, where.data);
}

function getInstructorsFromMaineStreet(courseId) {
	let sql = "Select USER_LMS_ID, TYPE From LMS_INSTRUCTORS_BY_CLASS ";
	const where = getCourseWhere(courseId);

	return db.queryDb(sql + where.clause, where.data);
}

function getCourseWhere(courseId) {
	const course = explodeCourseId(courseId);
	let whereData = [];
	whereData.push(
		course.institution,
		course.term,
		course.classNumber,
		course.isCombined
	);
	let whereClause =
		" Where " +
		" INSTITUTION = :INSTITUTION " +
		"And TERM = :TERM " +
		"And CLASS_NUMBER = :CLASS_NUMBER " +
		"And IS_COMBINED = :IS_COMBINED ";

	if (course.isCombined === "y") {
		if (course.sessionCode === null) {
			whereClause += " And SESSION_CODE Is null ";
		} else {
			whereData.push(course.sessionCode);
			whereClause += " And SESSION_CODE = :SESSION_CODE ";
		}
	}

	return {
		clause: whereClause,
		data: whereData
	};
}

function explodeCourseId(courseId) {
	let course = {};
	const splitCourse = courseId.split(".");

	course.term = splitCourse[0];
	course.classNumber = splitCourse[2];
	course.institution = splitCourse[1].substr(0, 5);

	course.sessionCode = null;
	if (splitCourse.length > 3) {
		course.sessionCode = splitCourse[3];
	}

	course.isCombined = "y";
	if (splitCourse[1].charAt(6) === "S") {
		course.isCombined = "n";
	}

	return course;
}

function updateCourseInLMS(lmsId, code, title) {
	return new Promise(function(resolve) {
		const lmsEntity = {
			code: code,
			name: title,
			isActive: false,
			cascadeChanges: false
		};

		helper.axios
			.put("/course/" + lmsId, lmsEntity)
			.then(function(res) {
				logger.info("Updated Course Successfully", lmsEntity);
			})
			.catch(function(err) {
				logger.warn("Error updating course ", {
					lmsEntity: lmsEntity,
					error: helper.handleAxiosError(err)
				});
			})
			.finally(function() {
				resolve();
			});
	});
}

function enrollInstructorIntoCourse(courseLmsId, userLmsId, enrollmentType) {
	return new Promise(function(resolve) {
		const lmsEntity = {
			courseLmsId: courseLmsId,
			userLmsId: userLmsId,
			type: enrollmentType
		};

		helper.axios
			.post("/enrollment", lmsEntity)
			.then(function(res) {
				logger.info("Enrolled Instructor Successfully", lmsEntity);
			})
			.catch(function(err) {
				logger.warn("Error enrolling instructor ", {
					lmsEntity: lmsEntity,
					error: helper.handleAxiosError(err)
				});
			})
			.finally(function() {
				resolve();
			});
	});
}
